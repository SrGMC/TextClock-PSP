local tables = require("tables")
local colors = {color.new(0, 0, 0), color.new(255, 255, 255), color.new(255, 0, 0), color.new(0, 255, 0), color.new(0, 0, 255), color.new(255, 255, 0),
                color.new(255, 0, 255), color.new(0, 255, 255), color.new(22, 160, 133), color.new(241, 196, 15), color.new(243, 156, 18),
                color.new(46, 204, 113), color.new(39, 174, 96), color.new(230, 126, 34), color.new(211, 84, 0), color.new(52, 152, 219),
                color.new(41, 128, 185), color.new(231, 76, 60), color.new(192, 57, 43), color.new(155, 89, 182), color.new(142, 68, 173),
                color.new(236, 240, 241), color.new(189, 195, 199), color.new(52, 73, 94), color.new(44, 62, 80), color.new(149, 165, 166),
                color.new(127, 140, 141)}

function colors.size()
    result = ((tables.length(colors)) - 1) / 2
    return result
end

-- Default colors
function colors.black()
    return colors[1]
end
function colors.white()
    return colors[2]
end
function colors.red()
    return colors[3]
end
function colors.green()
    return colors[4]
end
function colors.blue()
    return colors[5]
end
function colors.yellow()
    return colors[6]
end
function colors.magenta()
    return colors[7]
end
function colors.turquoise()
    return colors[8]
end

-- FlatUI color palette
function colors.greensea()
    return colors[9]
end
function colors.sunflower()
    return colors[10]
end
function colors.orange()
    return colors[11]
end
function colors.emerald()
    return colors[12]
end
function colors.nephritis()
    return colors[13]
end
function colors.carrot()
    return colors[14]
end
function colors.pumpkin()
    return colors[15]
end
function colors.peterriver()
    return colors[16]
end
function colors.belizehole()
    return colors[17]
end
function colors.alizarin()
    return colors[18]
end
function colors.pomegranate()
    return colors[19]
end
function colors.amethyst()
    return colors[20]
end
function colors.wisteria()
    return colors[21]
end
function colors.clouds()
    return colors[22]
end
function colors.silver()
    return colors[23]
end
function colors.wetasphalt()
    return colors[24]
end
function colors.midnightblue()
    return colors[25]
end
function colors.concrete()
    return colors[26]
end
function colors.asbestos()
    return colors[27]
end

return colors
