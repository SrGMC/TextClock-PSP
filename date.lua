local date = {}

function date.getDate()
    local current = os.getdate()
    return current
end

function date.getDayNum()
    local current = os.getdate()
    local d1 = string.byte(current, 1)
    local d2 = string.byte(current, 2)
    local d3 = string.byte(current, 3)
    local day = string.char(d1, d2, d3)

    if(day == "MON") then return 1
    elseif(day == "TUE") then return 2
    elseif(day == "WED") then return 3
    elseif(day == "THU") then return 4
    elseif(day == "FRI") then return 5
    elseif(day == "SAT") then return 6
    elseif(day == "SUN") then return 7 end
end

function date.getDay()
    local current = os.getdate()
    local d1 = string.byte(current, 5)
    local d2 = string.byte(current, 6)
    return tonumber(string.char(d1, d2))
end

function date.getMonth()
    local current = os.getdate()
    local m1 = string.byte(current, 8)
    local m2 = string.byte(current, 9)
    return tonumber(string.char(m1, m2))
end

function date.getMonthString()
    month = date.getMonth()
    months = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"}
    return months[month]
end

function date.getYear()
    local current = os.getdate()
    local y1 = string.byte(current, 11)
    local y2 = string.byte(current, 12)
    local y3 = string.byte(current, 13)
    local y4 = string.byte(current, 14)
    return tonumber(string.char(y1, y2, y3, y4))
end

function date.getHours()
    local current = os.getdate()
    local h1 = string.byte(current, 18)
    local h2 = string.byte(current, 19)
    return tonumber(string.char(h1, h2))
end

function date.getMinutes()
    local current = os.getdate()
    local m1 = string.byte(current, 21)
    local m2 = string.byte(current, 22)
    return tonumber(string.char(m1, m2))
end

function date.getSeconds()
    local current = os.getdate()
    local s1 = string.byte(current, 24)
    local s2 = string.byte(current, 25)
    return tonumber(string.char(s1, s2))
end

return date
