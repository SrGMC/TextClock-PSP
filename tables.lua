local tables = {}

function tables.length(t)
    if(type(t) == "table") then
        local count = 0
        for _ in pairs(t) do count = count + 1 end
        return count
    else return nil end
end

function tables.createMatrix(i, j, type)
    local default
    if (type == "number") then default = 0
    elseif (type == "string") then default = ""
    elseif (type == "boolean") then default = false
    else default = nil
    end

    matrix = {}
    for x=1, i do
        matrix[x] = {}
        for y=1, j do
            matrix[x][y] = default
        end
    end
    return matrix
end

return tables
